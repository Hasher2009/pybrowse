from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui
from PyQt5.QtWebEngineWidgets import *
import sys

class MainWindow(QMainWindow):
	def __init__(self):
		super(MainWindow, self).__init__()
		self.browser = QWebEngineView()
		self.browser.setUrl(QUrl('https://google.com'))
		self.setCentralWidget(self.browser)
		self.showMaximized()

		navbar = QToolBar()
		self.addToolBar(navbar)

		home_button = QAction('Google', self)
		home_button.triggered.connect(self.navigate_home2)
		navbar.addAction(home_button)

		back_button = QAction('Back', self)
		back_button.triggered.connect(self.browser.back)
		navbar.addAction(back_button)

		reload_button = QAction('Reload', self)
		reload_button.triggered.connect(self.browser.reload)
		navbar.addAction(reload_button)

		forward_button = QAction('Forward', self)
		forward_button.triggered.connect(self.browser.forward)
		navbar.addAction(forward_button)

		home_button = QAction('TryHackMe', self)
		home_button.triggered.connect(self.navigate_home)
		navbar.addAction(home_button)

		self.url_bar = QLineEdit()
		self.url_bar.returnPressed.connect(self.navigate_to_url)
		navbar.addWidget(self.url_bar)

		self.browser.urlChanged.connect(self.update_url)

	def navigate_home(self):
		self.browser.setUrl(QUrl('https://tryhackme.com'))
	def navigate_home2(self):
		self.browser.setUrl(QUrl('https://google.com'))
	def navigate_to_url(self):
		url = self.url_bar.text()
		self.browser.setUrl(QUrl(url))
	def update_url(self, q):
		self.url_bar.setText(q.toString())


app = QApplication(sys.argv)
QApplication.setWindowIcon(QtGui.QIcon('C:\\Users\\hp\\Desktop\\Logo Circle.png'))
QApplication.setApplicationName('PyBrowser')
window = MainWindow()
app.exec_()